# Laravel Streamed

A Laravel with Streams Platform already installed.

## Install

```bash
git clone git@gitlab.com:newebtime/pyrocms/laravel-streamed.git
cp .env.example .env

# Configure the database. Streams Platform cannot work without.

php artisan install --ready
```

## What changed from normal Laravel?

### Changed

* app/Console/Kernel.php
    * Extending SP Kernel
* app/Http/Kernel.php
    * Extending SP Kernel
    * Cleaning up the Application Kernel
* config/app.php
    * Registering SP ServiceProvider
* public/index.php
    * Wraping CacheKernel
* composer.json
    * Added `anomaly/streams-platform` and removed `laravel/framework`
    * Added `wikimedia/composer-merge-plugin`
    * Added `anomaly/streams-composer-plugin`
    * Added `merge-plugin` config in `extra`

### Added

* config/stream.php
* storage/httpcache

### Removed

Nothing
